package com.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.nio.file.Files;
import java.nio.file.Paths;

public class PayloadConverter {

    private static Logger log = LogManager.getLogger(PayloadConverter.class.getName());

    public static String generatePayloadString(String fileName) {
        log.info("Inside Payload Converter function");
        String filePath = System.getProperty("user.dir") + "\\resources\\" + fileName;
        try {
            return new String(Files.readAllBytes(Paths.get(filePath)));
        }catch (Exception e){
            log.error(e);
            e.printStackTrace();
            return null;
        }
    }
}
