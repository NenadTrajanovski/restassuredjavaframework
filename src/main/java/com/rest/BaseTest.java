package com.rest;

import com.util.PayloadConverter;
import com.util.TestUtils;
import com.util.URL;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BaseTest {

    private static Logger log = LogManager.getLogger(BaseTest.class.getName());

    public static String doLogin(){
        Response response;

        log.info("Staring test case: doLogin");
        String loginPayload = PayloadConverter.generatePayloadString("JiraLogin.json");

        String endPointURI = URL.getEndPoint("/rest/auth/1/session");
        response = RESTCalls.POSTRequest(endPointURI, loginPayload);
        log.info(response.getBody().asString());

        String strResponse = TestUtils.getResponseString(response);

        JsonPath jsonPath = TestUtils.jsonParser(strResponse);
        String sessionID = jsonPath.getString("session.value");
        log.info("JIRA SessionID: " + sessionID);
        return sessionID;
    }
}
