package com.rest;

import com.util.TestUtils;
import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;


public class Assertions {

    private static Logger log = LogManager.getLogger(Assertions.class.getName());

    public static void verifyTrue(boolean flag){
        Assert.assertTrue(flag);
    }

    public static void verifyFalse(boolean flag){
        Assert.assertFalse(flag);
    }

    public static void verifyStatus(Response response, int status ){
        Assert.assertEquals(TestUtils.getStatusCode(response), status);
    }

    public static void verifyStatus(Response response, String status ){
        Assert.assertEquals(TestUtils.getStatusMessage(response), status);
    }
}
