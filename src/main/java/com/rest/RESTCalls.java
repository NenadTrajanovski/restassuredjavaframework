package com.rest;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class RESTCalls {

    /**
     * This class will fire requests
     * DOCUMENTATION: https://docs.atlassian.com/software/jira/docs/api/REST/1000.1568.0/#api/2/
     */

    private static Logger log = LogManager.getLogger(RESTCalls.class.getName());

    public static Response GETRequest(String URI){
        log.info("Inside GETRequest call");
        RequestSpecification requestSpecification = RestAssured.given();
        requestSpecification.contentType(ContentType.JSON);
        log.debug(requestSpecification.log().all());
        return requestSpecification.post(URI);
    }

    public static Response POSTRequest(String URI, String strJSON){
        log.info("Inside POSTRequest call");
        RequestSpecification requestSpecification = RestAssured.given().body(strJSON);
        requestSpecification.contentType(ContentType.JSON);
        log.debug(requestSpecification.log().all());
        return requestSpecification.post(URI);
    }

    public static Response POSTRequest(String URI, String strJSON, String sessionID){
        log.info("Inside POSTRequest call");
        RequestSpecification requestSpecification = RestAssured.given().body(strJSON);
        requestSpecification.contentType(ContentType.JSON);
        requestSpecification.header("cookie", "JSESSIONID=" + sessionID + "");
        log.debug(requestSpecification.log().all());
        return requestSpecification.post(URI);
    }

    public static Response PUTRequest(String URI, String strJSON){
        log.info("Inside PUTRequest call");
        RequestSpecification requestSpecification = RestAssured.given().body(strJSON);
        requestSpecification.contentType(ContentType.JSON);
        log.debug(requestSpecification.log().all());
        return requestSpecification.put(URI);
    }

    public static Response DELETERequest(String URI, String strJSON){
        log.info("Inside DELETERequest call");
        RequestSpecification requestSpecification = RestAssured.given().body(strJSON);
        requestSpecification.contentType(ContentType.JSON);
        log.debug(requestSpecification.log().all());
        return requestSpecification.delete(URI);
    }
}
