import com.rest.BaseTest;
import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.Test;

public class LoginTests extends BaseTest {

    Response response;
    private static Logger log = LogManager.getLogger(LoginTests.class.getName());

    @Test
    public void loginFlow(){
        doLogin();
    }
}
