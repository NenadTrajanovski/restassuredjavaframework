import com.rest.Assertions;
import com.rest.BaseTest;
import com.rest.RESTCalls;
import com.util.PayloadConverter;
import com.util.URL;
import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class CreateIssueTests extends BaseTest{

    private String sessionID;
    Response response;

    private static Logger log = LogManager.getLogger(CreateIssueTests.class.getName());

    @BeforeMethod
    public void setUp(){
        sessionID = doLogin();
    }

    @Test
    public void createIssue(){
        log.info("Starting " + getClass());
        String URI = URL.getEndPoint("/rest/api/2/issue");
        String createIssuePayload = PayloadConverter.generatePayloadString("CreateBug.json");

        response = RESTCalls.POSTRequest(URI, createIssuePayload, sessionID);
        Assertions.verifyStatus(response, 201);
    }
}
